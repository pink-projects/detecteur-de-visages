import cv2 as cv
import sys

def load_cascades(face_cascade_path, eye_cascade_path):
    face_cascade = cv.CascadeClassifier(face_cascade_path)
    eye_cascade = cv.CascadeClassifier(eye_cascade_path)
    if face_cascade.empty() or eye_cascade.empty():
        #print("Erreur de chargement des classificateurs en cascade")
        sys.exit("Erreur de chargement des classificateurs en cascade")
    return face_cascade, eye_cascade

def load_image(image_path):
    img = cv.imread(image_path)
    if img is None:
        sys.exit("Erreur : Image non chargée. Vérifiez le chemin.")
    return img

def detect_faces(face_cascade, image):
    gray = cv.cvtColor(image, cv.COLOR_BGR2GRAY)
    faces = face_cascade.detectMultiScale(gray, 1.1, 8)
    return faces

def detect_eyes(eye_cascade, image):
    gray = cv.cvtColor(image, cv.COLOR_BGR2GRAY)
    eyes = eye_cascade.detectMultiScale(gray, 1.1, 3)
    return eyes

def draw_rectangles(image, faces, eyes):
    for x, y, w, h in faces:
        cv.rectangle(image, (x, y), (x + w, y + h), (150, 0, 255), 2)
    for ex, ey, ew, eh in eyes:
        cv.rectangle(image, (ex, ey), (ex + ew, ey + eh), (255, 0, 0), 2)
    return image


FACE_CASCADE_PATH = r'C:\Users\Admin\Documents\projects\detecteur visage\detecteur-de-visages\haarcascade_frontalface_default.xml'
EYE_CASCADE_PATH = r'C:\Users\Admin\Documents\projects\detecteur visage\detecteur-de-visages\haarcascade_eye.xml'
IMAGE_PATH = r'C:\Users\Admin\Documents\projects\detecteur visage\detecteur-de-visages\Rayan.jpeg'


face_cascade, eye_cascade = load_cascades(FACE_CASCADE_PATH, EYE_CASCADE_PATH)
    

img = load_image(IMAGE_PATH)
  

faces = detect_faces(face_cascade, img)
eyes = detect_eyes(eye_cascade, img)
img = draw_rectangles(img, faces, eyes)

cv.imshow('image principale', img)
cv.waitKey(0)
cv.destroyAllWindows()



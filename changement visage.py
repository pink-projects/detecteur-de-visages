import cv2 as cv
import sys

CASCADE_PATH = r'C:\Users\Admin\Documents\projects\detecteur visage\detecteur-de-visages\haarcascade_frontalface_default.xml'
IMAGE_PATH = input("Entrez le chemin de l'image à traiter:\n")

def load_resources(cascade_path, image_path):
    face_cascade = cv.CascadeClassifier(cascade_path)
    if face_cascade.empty():
        sys.exit("Erreur : Le classificateur en cascade n'a pas pu être chargé. Vérifiez le chemin.")

    img = cv.imread(image_path)
    if img is None:
        sys.exit("Erreur : Image non chargée. Vérifiez le chemin.")
    
    return face_cascade, img

def swap_faces(face_cascade, img):
    gray = cv.cvtColor(img, cv.COLOR_BGR2GRAY)
    faces = face_cascade.detectMultiScale(gray, 1.1, 8)
    if len(faces) != 2:
        sys.exit('La photo doit avoir exactement deux visages')

    x1, y1, w1, h1 = faces[0]
    x2, y2, w2, h2 = faces[1]
    face1 = img[y1:y1+h1, x1:x1+w1]
    face2 = img[y2:y2+h2, x2:x2+w2]
    face1_resized = cv.resize(face1, (w2, h2))
    face2_resized = cv.resize(face2, (w1, h1))
    img[y1:y1+h1, x1:x1+w1] = face2_resized
    img[y2:y2+h2, x2:x2+w2] = face1_resized

    return img

face_cascade, img = load_resources(CASCADE_PATH, IMAGE_PATH)
swapped_img = swap_faces(face_cascade, img)

SAVE_PATH = input("Entrez le chemin de sauvegarde pour l'image modifiée:\n")
cv.imwrite(SAVE_PATH, swapped_img)
print("Image sauvegardée avec succès à l'emplacement:", SAVE_PATH)

# README pour les Scripts de Manipulation et Détection des Visages

## Aperçu
Ce document décrit l'utilisation de deux scripts Python : "changement visage.py" et "visages.py". Le premier script permet d'intervertir deux visages sur une seule image, tandis que le second détecte les visages et les yeux dans les images.

## Fonctionnalités
- **changement visage.py** : Intervertit les visages de deux personnes sur une seule image. Utilisé pour des effets amusants ou des tests de reconnaissance faciale.
- **visages.py** : Détecte les visages et les yeux sur une image, utile pour des applications de surveillance, de sécurité ou de recherche scientifique.

## Prérequis
- Python 3.x
- Bibliothèques Python spécifiées dans le fichier `requirements.txt`.

## Installation
Pour préparer votre environnement à exécuter ces scripts, suivez les étapes ci-dessous :

1. **Installer Python** : Assurez-vous que Python 3.x est installé sur votre système.
2. **Dépendances** :
   Installez toutes les bibliothèques nécessaires en utilisant le fichier `requirements.txt`. Placez-vous dans le répertoire contenant ce fichier et exécutez la commande suivante :

   ```bash
   pip install -r requirements.txt
   ```

   Ce fichier `requirements.txt` contient les bibliothèques suivantes :
   - `numpy==1.26.4` : Pour le traitement numérique des images.
   - `opencv-python==4.9.0.80` : Pour le traitement et l'analyse d'images.

## Utilisation
Pour utiliser ces scripts, spécifiez les chemins vers les images contenant des visages. Les commandes pour exécuter chaque script sont :

```bash
python changement_visage.py <chemin_vers_image>
python visages.py <chemin_vers_image>
```

Remplacez `<chemin_vers_image>` par le chemin réel de votre image.

## Configuration
Configurez les paramètres tels que les chemins des images directement dans les scripts ou via des arguments de ligne de commande si cela est prévu.


## Exemples de resultats
### Changement généré avec `changement visage.py`
### image avant
![image avant](Rayan et autre.jpeg)
### image apres
![iamge après](Rayan et autre échange.jpeg)
### Changement généré avec `visages.py`
### image avant
![image avant](Rayan.jpeg)
### image apres
![image après](Rayan detecté.png)